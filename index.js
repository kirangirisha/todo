var arr_container = [];
var count = 0;
var obj = {};

document.getElementById("add").addEventListener('click', function () {
    var element = document.getElementById('item').value;
    if (element) {
        addItemTodo(element);
        document.getElementById('item').value = "";
    }
});

document.getElementById("item").addEventListener("keyup", function (event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("add").click();
    }
});

function addItemTodo(item) {
    count += 1;
    obj = {
        "count": count,
        "todo": item,
        "task_completed": false
    }
    arr_container.push(obj);
    refresh(count, "add");
}

function refresh(count, action) {
    if (action == "add") {
        var chk_todo = "chk" + count;
        var input_todo = "input_text" + count;
        var uList = document.getElementById("content-list");

        for (var i = 0; i < arr_container.length; i++) {
            var ret_val = arr_container[i].todo;

            var create_li = document.createElement('ul');
            var ilist = `<input class="item_chk" type="checkbox" onclick=update_todo("${count}"); id="${chk_todo}"></input>
                    <label class="text" id="${input_todo}">${ret_val}</label>
                    <i class="fa fa-trash-o de" onclick=removeItem("${count}");></i>`
        }
        create_li.innerHTML = ilist;
        uList.appendChild(create_li);
    } else if (action == "del") {
        var val = document.getElementById("chk" + String(count));
        var pt_val = val.parentElement;
        pt_val.remove();
    } else {
        alert("Invalid action")
    }
}

function removeItem(del_todo) {
    for (i = 0; i < arr_container.length; i++) {
        if (String(arr_container[i].count) === String(del_todo)) {
            arr_container.pop(i);
            refresh(del_todo, "del");
        }
    }
}

function update_todo(count) {
    var ele = document.getElementById("input_text" + String(count).trim());
    if (document.getElementById("chk" + String(count).trim()).checked) {
        ele.style.textDecoration = "line-through";
        ele.style.color = "grey";
    } else {
        ele.style.textDecoration = "none";
        ele.style.color = "black";
    }
}